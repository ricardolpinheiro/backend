<?php

use App\Models\Admin;
use App\Models\Bouquet;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);

        $bouquets = [
            "Musica",
            "Infantil",
            "Religioso",
            "Variedades",
            "Canais Adultos",
            "Noticias",
            "Filmes & Series",
            "Canais Abertos",
            "Esportes",
            "Canais",
            "Filmes - Séries - Desenhos VOD",
            "Series VOD",
            "Cameras ao Vivo",
            "Documentários",
            "Desenhos VOD",
            "Canais sem Adultos",
            "Radios",
            "Big Brother Brasil",
            "Kids",
        ];

        $user = Admin::where('username', '=', 'ricardo')->get();

        if(!$user->count()) {
            Admin::create([
                'name' => "Ricardo",
                'username' => "ricardo",
                'password' => Hash::make("ricardo"),
            ]);
        }

        foreach ($bouquets as $b) {
            $bouquet = new Bouquet();
            $bouquet->name = $b;
            $bouquet->status = 1;
            $bouquet->save();
        }

    }
}
