<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExportTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('export', function (Blueprint $table) {
            $table->id();
            $table->string('type_key')->nullable();
            $table->string('stream_display_name')->nullable();
            $table->string('stream_source')->nullable();
            $table->string('stream_icon')->nullable();
            $table->string('channel_id')->nullable();
            $table->string('movie_propeties')->nullable();
            $table->string('title')->nullable();
            $table->string('tmdb_id')->nullable();
            $table->string('season')->nullable();
            $table->string('episode')->nullable();
            $table->string('link')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp_series');
    }
}
