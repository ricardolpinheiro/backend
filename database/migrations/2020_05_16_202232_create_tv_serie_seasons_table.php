<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTvSerieSeasonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tv_serie_seasons', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid');
            $table->uuid('tv_series_id')->nullable();
            $table->integer('tmdb_id')->nullable();
            $table->bigInteger('season_no');
            $table->string('poster_path')->nullable();
            $table->text('detail')->nullable();
            $table->char('type')->default('S');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tv_serie_seasons');
    }
}
