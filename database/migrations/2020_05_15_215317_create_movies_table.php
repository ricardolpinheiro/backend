<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMoviesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movies', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid');
            $table->integer('tmdb_id')->nullable();
            $table->string('title')->nullable();
            $table->uuid('category_id')->nullable();
            $table->string('movie_link');
            $table->boolean('adult')->nullable();
            $table->string('duration')->nullable();
            $table->string('thumbnail')->nullable();
            $table->string('poster')->nullable();
            $table->uuid('collection_id')->nullable();
            $table->string('trailer_url')->nullable();
            $table->text('detail')->nullable();
            $table->string('status')->nullable()->default('pending');
            $table->integer('rating')->nullable();
            $table->string('released')->nullable();
            $table->char('type')->default('M');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movies');
    }
}
