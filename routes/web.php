<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function() {
    //RESOURCES
    Route::resource('admins', 'Admin\\AdminController');
    Route::resource('revendas', 'Admin\\RevendaController');
    Route::resource('bouquets', 'Admin\\BouquetController');
    Route::resource('categories', 'Admin\\CategoryStreamController');
    Route::resource('hosts', 'Admin\\HostStreamController');
    Route::resource('streams', 'Admin\\StreamController');
    Route::resource('movies', 'Admin\\MovieController');
    Route::resource('radios', 'Admin\\RadioController');
    Route::resource('series', 'Admin\\SerieController');
    Route::resource('seasons', 'Admin\\SeasonController');
    Route::resource('episodes', 'Admin\\EpisodeController');

    //DATATABLES
    Route::get('datatable/admins', 'Admin\\AdminController@datatable')->name('admins.datatable');
    Route::get('datatable/revendas', 'Admin\\RevendaController@datatable')->name('revendas.datatable');
    Route::get('datatable/bouquets', 'Admin\\BouquetController@datatable')->name('bouquets.datatable');
    Route::get('datatable/categories', 'Admin\\CategoryStreamController@datatable')->name('categories.datatable');
    Route::get('datatable/hosts', 'Admin\\HostStreamController@datatable')->name('hosts.datatable');
    Route::get('datatable/streams', 'Admin\\StreamController@datatable')->name('streams.datatable');
    Route::get('datatable/movies', 'Admin\\MovieController@datatable')->name('movies.datatable');
    Route::get('datatable/radios', 'Admin\\RadioController@datatable')->name('radios.datatable');
    Route::get('datatable/series', 'Admin\\SerieController@datatable')->name('series.datatable');
    Route::get('datatable/seasons', 'Admin\\SeasonController@datatable')->name('seasons.datatable');
    Route::get('datatable/episodes', 'Admin\\EpisodeController@datatable')->name('episodes.datatable');


    //AUX
    Route::post('password/admins', 'Admin\\AdminController@password')->name('admins.password');
    Route::get('/bouquets/relation/{res}/{uuid}', 'Admin\\BouquetRelationController@index');
    Route::get('/stream/status/{uuid}', 'Admin\\StreamController@status');


});
Route::get('/teste', 'HomeController@teste');
Route::get('/sync', 'Api\\ServiceController@fetch');
