<div class="form-group {{ ($hasError == true) ? "has-error has-danger" : '' }}">
    <label for="{{ $name }}">{{ __(ucfirst($text ?? $name)) }}</label>
    <div class="date-input">
        <input type="{{ $type }}" value="{{ $value }}" class="single-daterange form-control" name="{{ $name }}" placeholder="{{ $placeholder }}" autocomplete="{{ $name }}" @if($required) required @endif>
    </div>
    @if($hasError)
        <div class="help-block form-text text-muted form-control-feedback">{{ $message }}</div>
    @endif
</div>
