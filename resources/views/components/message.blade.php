<div class="alert alert-success" role="alert">
    <strong>Sucesso! </strong>
    {{ __($message) }}
</div>
