<div class="form-group {{ ($hasError == true) ? "has-error has-danger" : '' }}">
    <label for="{{ $name }}">{{ __(ucfirst($text ?? $name)) }}</label>
    <div class="input-group">
        <div class="input-group-prepend">
            <div class="input-group-text">{{$prepend}}</div>
        </div>
        <input type="text" value="{{ $value }}" class="form-control" name="{{ $name }}" placeholder="{{ $placeholder }}" autocomplete="{{ $name }}" @if($required) required @endif>

    </div>
    @if(!empty($icon))
        <div class="pre-icon os-icon {{ $icon }}"></div>
    @endif
    @if($hasError)
        <div class="help-block form-text text-muted form-control-feedback">{{ $message }}</div>
    @endif
</div>
