<div aria-hidden="true" aria-labelledby="exampleModalLabel" class="modal fade" id="modalStream" role="dialog" tabindex="-1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    Status de Stream
                </h5>
                <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table table-lightborder">
                        <thead>
                        <tr>
                            <th>Links</th>
                            <th>Status</th>
                        </tr>
                        </thead>
                        <tbody id="content-stream">

                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" data-dismiss="modal" type="button"> Fechar</button>
            </div>
        </div>
    </div>
</div>
<script>
    axios.defaults.headers.common = {
        'X-Requested-With': 'XMLHttpRequest',
        'X-CSRF-TOKEN' : document.querySelector('meta[name="csrf-token"]').getAttribute('content')
    };

    $('#modalStream').on('hidden.bs.modal', function (e) {
        $("#content-stream").empty()
    })

    function showLink(uuid) {
        $("#modalStream").modal('toggle')

        up = '';


        return axios.get(`/stream/status/${uuid}`).then(function (response) {

            if(Object.values(response.data['data']).length > 0){
                $.each(Object.values(response.data['data']), function( index, value ) {

                    col = '';
                    down = '<div style="cursor:pointer" class="badge badge-danger-inverted"><i class="os-icon os-icon-arrow-down-circle"></i> Down</div>';
                    up = '<div style="cursor:pointer" class="badge badge-success-inverted"><i class="os-icon os-icon-arrow-up-circle"></i> Up</div>';
                    if(value.status === 'up') {
                        col = up;
                    } else {
                        col = down;
                    }

                    $("#content-stream").append(`<tr><td>${value.link}</td><td>${col}</td></tr>`)
                });
            }
        })
    }
</script>
