<x-input name="name" text="Nome" :value="$model->name ?? ''" placeholder="Digite o Nome da Stream" :error="$errors"/>

<x-select2 name="category_id" text="Categoria" :value="$model->category_id ?? ''" placeholder="Selecione a Categoria" :error="$errors"
           :options="$categories"/>

<x-select2 name="epg_id" text="EPG" placeholder="Selecione o Canal" :value="$model->epg_id ?? ''" :error="$errors"
           :options="$epgs"/>

<x-select2 name="host_id" text="Fornecedor de Host" placeholder="Selecione o Host" :value="$model->host_id ?? ''" :error="$errors"
           :options="$hosts"/>

<x-multiselect name="bouquet_id[]" text="Bouquets" :value="$model->available_bouquets ?? ''" placeholder="Selecione o(s) Bouquet(s)" :error="$errors"
               :options="$bouquets"/>

@if(isset($model))
    <div class="profile-tile">
        <a class="profile-tile-box" href="{{ $model->stream_logo ?? '' }}" target="_blank">
            <div class="pt-avatar-w">
                <img alt="" src="{{ $model->stream_logo ?? ''}}">
            </div>
        </a>
        <div class="pt-btn">
            <button type="button" class="btn btn-secondary btn-sm" onclick="$('#stream_logo').trigger('click');">Alterar</button>
        </div>
    </div>
@endif

@if(isset($model))
    <x-input-file display="none" name="stream_logo" text="Stream Logo" :value="$model->stream_logo ?? ''" placeholder="" :error="$errors"/>
@else
    <x-input-file name="stream_logo" text="Stream Logo" placeholder="" :error="$errors"/>
@endif

<x-text-area name="notes" text="Observação" :value="$model->notes ?? ''" placeholder="Digite o texto" :error="$errors"/>
