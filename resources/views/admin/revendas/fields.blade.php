<x-legend text="Dados de Acesso"/>

<x-input name="name" text="Nome" placeholder="Digite o Nome" :error="$errors"/>

<x-input name="username" text="Username" placeholder="Digite o Username" :error="$errors"/>

<x-input name="password" text="Senha" placeholder="Digite a Senha" :error="$errors" />

<x-multiselect name="bouquet_id[]" text="Bouquets" placeholder="Selecione o(s) Bouquet(s)" :error="$errors"
               :options="$bouquets"/>

<x-legend text="Configurações"/>

<fieldset class="form-group">
    <x-input name="app_name" text="Nome do App" placeholder="Digite o Nome do App" :error="$errors"/>

    <x-input-file name="path_logo" text="Logo da Revenda" placeholder="" :error="$errors"/>

    <x-input-file name="path_background" text="Background da Revenda" placeholder="" :error="$errors"/>

    <div class="row">
        <div class="col-md-6 no-padding-content">
            <x-input-prepend prepend="#" value="1c4cc3" name="primary_color" text="Cor Primária" placeholder="Insira a Cor Primaria" :error="$errors"/>
        </div>
        <div class="col-md-6">
            <x-input-prepend prepend="#" value="1c4cc3" name="secondary_color" text="Cor Secundária" placeholder="Insira a Cor Secundária" :error="$errors"/>
        </div>
    </div>

    <x-input type="number" value="99999" name="limit_users" text="Limite de Usuários" placeholder="Digite o Limite" :error="$errors" />

    <x-text-area name="obs" text="Observação" placeholder="Digite o texto" :error="$errors"/>

</fieldset>
