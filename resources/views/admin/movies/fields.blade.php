<x-input name="title" text="Título" :value="$model->title ?? ''" placeholder="Digite o Titulo do Filme" :error="$errors"/>

<x-input name="tmdb_id" text="TMDB ID" :value="$model->tmdb_id ?? ''" placeholder="ID do Filme no TMDB" :error="$errors"/>

<x-multiselect name="bouquet_id[]" text="Bouquets" :value="$model->available_bouquets ?? ''" placeholder="Selecione o(s) Bouquet(s)" :error="$errors"
               :options="$bouquets"/>

<x-select2 name="category_id" text="Categoria" :value="$model->category_id ?? ''" placeholder="Selecione a Categoria" :error="$errors"
           :options="$categories"/>

<x-input-prepend prepend="http://" name="movie_link" text="Filme URL" :value="$model->movie_link ?? ''" placeholder="Insira a URL do Filme" :error="$errors"/>
