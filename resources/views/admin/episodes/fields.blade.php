<x-input name="title" text="Titulo" placeholder="Digite o Titulo" :error="$errors"/>

<x-input type="number" name="episode_no" text="Episódio" placeholder="Digite o Episódio" :error="$errors"/>

<x-select2 name="seasons_id" text="Temporada" placeholder="Selecione a Temporada" :error="$errors"
           :options="$seasons"/>

<x-input name="episode_link" text="Link do Episído" placeholder="Digite link do Episódio" :error="$errors"/>

<x-select2 name="category_id" text="Categoria" placeholder="Selecione a Categoria" :error="$errors"
           :options="$categories"/>

<x-text-area name="detail" text="Overview" placeholder="Digite o texto" :error="$errors"/>
