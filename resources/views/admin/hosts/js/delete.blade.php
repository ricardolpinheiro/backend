<script>
    function deletResource(id) {
        Swal.fire({
            title: 'Tem certeza?',
            text: "O {{getTitlePage($res)}} será deletado da base",
            icon: 'warning',
            showCancelButton: true,
            cancelButtonText: "Cancelar",
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sim Deletar'
        }).then((result) => {
            if (result.value) {
                $("#id-form-delete").attr('action', '/{{$res}}/' + id);
                $("#form-status").val(status);
                $("#id-form-delete").submit();
            }
        })
    }
</script>
