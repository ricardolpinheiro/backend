<div aria-hidden="true" aria-labelledby="exampleModalLabel" class="modal fade" id="exampleModal1" role="dialog" tabindex="-1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    Bouquets
                </h5>
                <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table table-lightborder">
                        <thead>
                        <tr>
                            <th>Bouquet</th>
                        </tr>
                        </thead>
                        <tbody id="content-bouquet">

                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" data-dismiss="modal" type="button"> Fechar</button>
            </div>
        </div>
    </div>
</div>
<script>
    axios.defaults.headers.common = {
        'X-Requested-With': 'XMLHttpRequest',
        'X-CSRF-TOKEN' : document.querySelector('meta[name="csrf-token"]').getAttribute('content')
    };

    $('#exampleModal1').on('hidden.bs.modal', function (e) {
        $("#content-bouquet").empty()
    })

    function showBouquets(uuid, res) {
        $("#exampleModal1").modal('toggle')

        return axios.get(`/bouquets/relation/${res}/${uuid}`).then(function (response) {
            if(response.data.length > 0){
                console.log();
                $.each(response.data, function( index, value ) {
                    $("#content-bouquet").append(`<tr><td>${value}</td></tr>`)
                });
            }
        })
    }
</script>
