<x-input name="title" text="Título" :value="$model->title ?? ''" placeholder="Digite o Título" :error="$errors"/>

<x-input name="tmdb_id" text="TMDB ID" :value="$model->tmdb_id ?? ''" placeholder="Digite o TMDB ID" :error="$errors"/>

<x-multiselect name="bouquet_id[]" text="Bouquets" :value="$model->available_bouquets ?? ''" placeholder="Selecione o(s) Bouquet(s)" :error="$errors"
               :options="$bouquets"/>

<x-text-area name="detail" text="Overview" :value="$model->detail ?? ''" placeholder="Digite o texto" :error="$errors"/>

<x-input type="number" name="rating" text="Rating" :value="$model->rating ?? ''" placeholder="Digite o Título" :error="$errors"/>

@if(isset($model))
    <div class="profile-tile">
        <a class="profile-tile-box" href="{{ $model->poster_path ?? '' }}" target="_blank">
            <div class="pt-avatar-w">
                <img alt="" src="{{ $model->poster_path ?? ''}}">
            </div>
        </a>
        <div class="pt-btn">
            <button type="button" class="btn btn-secondary btn-sm" onclick="$('#poster_path').trigger('click');">Alterar</button>
        </div>
    </div>
    <x-input-file display="none" id="poster_path" name="poster_path" text="Poster" :value="$model->poster_path ?? ''" placeholder="" :error="$errors"/>
@else
    <x-input-file name="poster_path" text="Poster" placeholder="" :error="$errors"/>

@endif


@if(isset($model))
    <div class="profile-tile">
        <a class="profile-tile-box" href="{{ $model->backdrop_path ?? '' }}" target="_blank">
            <div class="pt-avatar-w">
                <img alt="" src="{{ $model->backdrop_path ?? ''}}">
            </div>
        </a>
        <div class="pt-btn">
            <button type="button" class="btn btn-secondary btn-sm" onclick="$('#backdrop_path').trigger('click');">Alterar</button>
        </div>
    </div>
    <x-input-file display="none" id="backdrop_path" name="backdrop_path" text="Background" :value="$model->backdrop_path ?? ''" placeholder="" :error="$errors"/>
@else
    <x-input-file name="backdrop_path" text="Background" :value="$model->backdrop_path ?? ''" placeholder="" :error="$errors"/>
@endif
