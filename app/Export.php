<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Export extends Model
{
    public $timestamps = false;

    protected $table = 'export';

    protected $fillable = [
        "type_key",
        "stream_display_name",
        "stream_source",
        "stream_icon",
        "channel_id",
        "movie_propeties",
        "title",
        "tmdb_id",
        "season",
        "episode",
        "link",
    ];

}
