<?php


use App\Jobs\TmdbFetchEpisode;
use App\Jobs\TmdbFetchMovie;
use App\Jobs\TmdbFetchSeason;
use App\Jobs\TmdbFetchSeason as TmdbFetchSeasonAlias;
use App\Jobs\TmdbFetchSerie;
use App\Models\TvSerie;
use App\Models\TvSerieEpisode;
use App\Models\TvSerieSeason;
use Gumlet\ImageResize;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;
use Ixudra\Curl\Facades\Curl;

function getTitlePage($res) {
    $arr['admins'] = "Administradores";
    $arr['revendas'] = "Revendas";
    $arr['bouquets'] = "Bouquets";
    $arr['categories'] = "Categorias de Stream";
    $arr['hosts'] = "Host de Stream";
    $arr['streams'] = "Stream";
    $arr['movies'] = "Filmes";
    $arr['radios'] = "Radios";
    $arr['series'] = "Séries de TV";
    $arr['seasons'] = "Temporadas de Séries de TV";
    $arr['episodes'] = "Episódios de Séries de TV";


    return $arr[$res] ?? '';
}

function parseSeasontoArray() {
    $season = TvSerieSeason::all();
    $res = ['' => 'Selecione'];

    foreach ($season as $season) {
        $res[$season->uuid] = $season->serie->title . " - Season: " .$season->season_no;
    }
    return $res;
}

function btnTableEdit($route, $model) {
    return '<a href="'.route($route, $model->uuid).'" class="btn btn-xs btn-primary" ><i class="os-icon os-icon-ui-49"></i></a>';
}

function btnTableActive($model) {
    return '<button onclick="activeResouce(\''.$model->uuid.'\')" class="btn btn-xs btn-secondary"><i class="os-icon os-icon-cloud"></i></button>';
}

function btnTableInactive($model) {
    return '<button onclick="inactiveResouce(\''.$model->uuid.'\')" class="btn btn-xs btn-secondary"><i class="os-icon os-icon-cloud-off"></i></button>';
}

function btnTableDelete($route, $item) {
    return '<button onclick="deletResource(\''.$item->uuid.'\')" class="btn btn-xs btn-danger"><i class="os-icon os-icon-cancel-square"></i></button>';
}

function btnTablePassword($item) {
    return '<button onclick="setPassword(\''.$item->uuid.'\')" data-id="'.$item->uuid.'" class="btn btn-xs btn-info"><i class="os-icon os-icon-lock"></i></button>';
}

function bedgeHtml($type, $text) {
    return '<div class="badge badge-'.$type.'-inverted">'.$text.'</div>';
}

function imgOnTable($url) {
    return '<div class="cell-img" onclick="window.open(\''.$url.'\',\'mywindow\');" style="background-image: url('.$url.');display: inline-block;width: 30px;height: 30px;background-size: cover;background-position: center center;border-radius: 2px;box-shadow: 0px 0px 0px 2px #fff,1px 1px 5px rgba(0,0,0,0.8);vertical-align: middle;-webkit-transition: all 0.1s ease;transition: all 0.1s ease;-webkit-transform: scale(1);transform: scale(1);position: relative;cursor: pointer;"></div>';
}

function bouquetOnTable($model, $res){
    return '<button onclick="showBouquets(\''.$model->uuid.'\', \''.$res.'\')" class="mr-2 mb-2 btn btn-outline-secondary" type="button"><i class="os-icon os-icon-menu"></i></button>';
}

function bedgeHtmlIcon($type, $text, $icon, $model) {
    return '<div style="cursor:pointer" onclick="showLink(\''.$model->uuid.'\')" class="badge badge-'.$type.'-inverted"><i class="os-icon '.$icon.'"></i> '.$text.'</div>';
}

function bedgeTableStatusClick($status, $model) {
    switch ($status){
        case 2:
            return bedgeHtmlIcon('danger', 'Down', 'os-icon-arrow-down-circle', $model);
        break;
        case 3:
            return bedgeHtmlIcon('success', 'Up', 'os-icon-arrow-up-circle', $model);
        break;
        case 4:
            return bedgeHtmlIcon('info', 'Warning', 'os-icon-alert-circle', $model);
        break;
    }
}
function bedgeTableStatus($status) {
    switch ($status){
        case 0:
            return bedgeHtml('danger', 'Inativo');
            break;
        case 1:
            return bedgeHtml('success', 'Ativo');
            break;

    }
}

function parseSelectArray($obj, $k, $conc = false, $par = '') {
    $res = ['' => 'Selecione'];
    foreach ($obj as $key => $item) {
        if($conc) {
            $res[$item['uuid']] = $item[$par] . ' - ' . $item[$k];
        } else {
            $res[$item['uuid']] = $item[$k];
        }

    }
    return $res;
}

//Armazena imagem que vem de uma Request
function storage($request, $name, $dir, $public = true) {
    $image = $request->file($name);
    $imageName = time() . '.' . $image->getClientOriginalExtension();
    $destinationPath = ($public) ? public_path($dir) : storage_path($dir);
    $image->move($destinationPath, $imageName);
    $image->imagePath = $destinationPath . $imageName;
    return url($dir . $imageName);
}

function storage_resource($url, $path, $width = 300) {
    $contents = @file_get_contents($url);
    if($contents) {
        $name = $path .  preg_replace('/\?.*/', '', substr($url, strrpos($url, '/') + 1));
        Storage::put($name, $contents);
        try{
            $image = new ImageResize(storage_path("app" . $name));
            $image->resizeToWidth($width);
            $image->save(storage_path("app" . $name));
            return URL::to(str_replace('/public', '/storage', $name));
        } catch (\Gumlet\ImageResizeException $e) {
            return URL::to(str_replace('/public', '/storage', $name));
        }
    } else {
        return URL::to(public_path('img/electronics.png'));
    }

}

function checkExistsUrl($url) {
    $response = Curl::to($url)
        ->returnResponseObject()
        ->withTimeout(0)
        ->asJson()
        ->get();

    if($response->status == 200) {
        return true;
    } else {
        return false;
    }
}

function fetch($url) {
    $response = Curl::to($url)
        ->returnResponseObject()
        ->withTimeout(0)
        ->asJson()
        ->get();

    if($response->status == 200) {
        return $response->content;
    } else {
        return false;
    }
}

function tmdbGetMovieById($tmdb_id) {
    $url = str_replace(REPLACEMENT_ID, $tmdb_id, TMDB_API_FIND_BY_ID);
    return fetch($url);
}

function tmdbGetMovieByName($title) {
    $url = str_replace(REPLACEMENT_NAME, urlencode($title),TMDB_API_FIND_BY_NAME);
    return fetch($url);
}

function tmdbGetSerieByName($title) {
    $url = str_replace(REPLACEMENT_NAME, urlencode($title), TMDB_API_FIND_SERIE_BY_NAME);
    return fetch($url);
}

function tmdbGetCollectionById($id) {
    $url = str_replace(REPLACEMENT_ID, urlencode($id),TMDB_API_FIND_COLLECTION);
    return fetch($url);
}

function tmdbGetSerieById($tmdb_id) {
    $url = str_replace(REPLACEMENT_ID, $tmdb_id, TMDB_API_FIND_SERIE_BY_ID);
    return fetch($url);
}

function tmdbGetSeasonBySerie($serie, $season) {
    $url = str_replace(REPLACEMENT_ID, urlencode($serie), TMDB_API_FIND_SEASON_BY_SERIE);
    $url = str_replace(REPLACEMENT_SEASON, $season, $url);
    return fetch($url);
}

function tmdbGetEpisodeBySerie($serie, $season, $episode) {
    $url = str_replace(REPLACEMENT_ID, urlencode($serie), TMDB_API_FIND_EPISODE_BY_SERIE);
    $url = str_replace(REPLACEMENT_SEASON, $season, $url);
    $url = str_replace(REPLACEMENT_EPISODE, $episode, $url);
    return fetch($url);
}

function checkExistsSerie($s) {

    if(isset($s['name'])) {
        $data = TvSerie::where('title', '=', trim($s['name']))->orWhere('original_name', trim($s['name']))->get();


        if($data->count() == 0 && !empty($s['name'])) {
            $serie = tmdbGetSerieByName(trim($s['name']));

            if($serie->total_results > 0) {
                $find = TvSerie::where('title', '=', $serie->results[0]->name)->orWhere('original_name', $serie->results[0]->name)->get();

                if($find->count() == 0) {
                    if($serie->total_results > 0) {
                        $create['tmdb_id'] = $serie->results[0]->id;
                        $saved = TvSerie::create($create);
                        dispatch(new TmdbFetchSerie($saved));
                        return $saved;
                    }
                }
            }
        } elseif(!empty($s['name'])) {
            return $data[0];
        } else {
            Log::info("Erro" ,$s);
        }
    } else {
        Log::info("Erro" ,$s);
    }
}

function checkExistsSerieSeason (TvSerie $serie, $s) {
    $season = $serie->seasons()->where('season_no', '=', $s['temporada'])->get();

    if($season->count() == 0) {
        $temp['tv_series_id'] = $serie->uuid;
        $temp['season_no'] = $s['temporada'];
        $saved = TvSerieSeason::create($temp);

        dispatch(new TmdbFetchSeason($saved));
        return $saved;
    } else {
        return $season[0];
    }
}
//TODO temporaário para importação de dados - Episode
function checkExistsSerieSeasonepisode(TvSerieSeason $season, $serie) {
    $episode = $season->episodes()->where('episode_no', '=', $season['episodio'])->get();

    if($episode->count() == 0) {
        $ep['seasons_id'] = $season->uuid;
        $ep['episode_no'] = $serie['episodio'];
        $ep['episode_link'] = $serie['link'];
        $saved = TvSerieEpisode::create($ep);
        dispatch(new TmdbFetchEpisode($season->uuid, $saved->uuid));
    }
}

?>
