<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Bouquet;
use App\Models\BouquetRelation;
use App\Models\CategoryStream;
use App\Models\HostStream;
use App\Models\Stream as Resource;
use App\Models\StreamLink;
use App\Models\StreamStatus;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class StreamController extends Controller
{
    private $res = 'streams';

    private $categories;
    private $bouquets;
    private $epgs;
    private $hosts;

    public function __construct()
    {
        $categories = CategoryStream::all();
        $categories = $categories->toArray();

        $bouquets = Bouquet::where('status', 1)->get();
        $bouquets = $bouquets->toArray();

//        $epgs = EpgChannel::all();
//        $epgs = $epgs->toArray();

        $hosts = HostStream::all();
        $hosts = $hosts->toArray();

        $this->categories =  parseSelectArray($categories, 'name');
        $this->bouquets =  parseSelectArray($bouquets, 'name');
        $this->hosts =  parseSelectArray($hosts, 'host_name');
//        $this->epgs =  parseSelectArray($epgs, 'nome', true, 'id_canal');;
        $this->epgs =  [];
    }

    public function index()
    {
        $res = $this->res;
        return view('admin.'.$this->res.'.index')->with(compact('res'));
    }

    public function status($uuid) {
        $links = [];
        $links['data'] = [];

        $stream = Resource::where('uuid', '=', $uuid)->first();

        foreach ($stream->links()->get() as $link) {
            $links['data'][$link->uuid] = [];
            foreach ($link->status()->get() as $status) {
                $stat = (is_null($status->status)) ? 'down' : $status->status;
                $links['data'][$link->uuid]['link'] = $link->url;
                $links['data'][$link->uuid]['status'] = $stat;
            }

            if(!isset($links[$link->uuid]['link'])){
                $links['data'][$link->uuid]['link'] = $link->url;
                $links['data'][$link->uuid]['status'] = 'down';
            }
        }

        return $links;
    }

    public function datatable(Request $request) {

        $model = Resource::select(Resource::$cols)->get();
        return Datatables::of($model)
            ->addColumn('action', function ($model) {
                $btn = btnTableEdit($this->res.'.edit', $model);
                //$btn .= ($model->status === 0) ? btnTableActive($model) : btnTableInactive($model);
                $btn .= btnTableDelete($this->res.'.destroy', $model);
                return $btn;
            })
            ->editColumn('status', function ($model) {
                $stream = Resource::where('uuid', '=', $model->uuid)->first();
                $status = [];

                foreach ($stream->links()->get() as $link){

                    $all = StreamStatus::where('link_id', '=', $link->uuid);

                    if(!$all->count()) {
                        array_push($status, '0');
                    } else {

                        if($all->first()->status === 'up') {
                            array_push($status, '1');
                        } else {
                            array_push($status, '0');
                        }

                    }
                }
                $unique = array_unique($status);
                //[0]
                //[0,1]

                $final_status = 0;
                $diff = false;

                if(count($unique) > 1){
                    $final_status = 4;
                } else {

                    if(in_array('0', $unique)) {
                        $final_status = 2;
                    } else {
                        $final_status = 3;
                    }
                }

                return bedgeTableStatusClick($final_status, $stream);
            })
            ->rawColumns(['status', 'action'])
            ->make(true);
    }

    public function create()
    {
        $res = $this->res;
        $categories = $this->categories;
        $bouquets = $this->bouquets;
        $epgs = $this->epgs;
        $hosts = $this->hosts;
        return view('admin.'.$this->res.'.create')->with(compact('res','categories', 'bouquets', 'epgs', 'hosts'));
    }

    public function store(Request $request)
    {
        $res = $this->res;

        $request->validate([
            'name' => 'required',
            'stream_logo' => 'required|mimes:jpeg,bmp,png,jpg',
            'category_id' => 'required|exists:category_streams,uuid',
            'link' => 'required|array|min:1',
            "link.*" => 'required|url',
            'bouquet_id' => 'array',
            'bouquet_id.*' => 'exists:bouquets,uuid'
        ]);

        $input = $request->all();

        if($request->hasFile('stream_logo')) {
            $input['stream_logo'] = storage($request, 'stream_logo', '/img/stream/');
        }

        $model = Resource::create($input);
        if($model && isset($input['bouquet_id'])) {
            foreach ($input['bouquet_id'] as $bouquet){
                BouquetRelation::create(['resource_id' => $model->uuid, 'resource' => 'stream', 'bouquet_id'=> $bouquet]);
            }
        }

        foreach ($request->get('link') as $url) {
            StreamLink::create(['stream_id' => $model->uuid, "url" => $url]);
        }

        return redirect(route($this->res.'.index'))->with('success', __('messages.'.$this->res.'.created'));
    }

    public function show($id)
    {
        $res = $this->res;
        $model = Resource::where('uuid', '=', $id)->first();

        if (empty($model)) {
            return redirect(route($this->res.'.index'))->with('error', __('messages.'.$this->res.'.not_found'));
        }

        return view('admin.'.$this->res.'.show')->with(compact('model'));
    }

    public function edit($id)
    {
        $res = $this->res;
        $categories = $this->categories;
        $bouquets = $this->bouquets;
        $epgs = $this->epgs;
        $hosts = $this->hosts;
        $model = Resource::where('uuid', '=', $id)->first();

        if (empty($model)) {
            return redirect(route($this->res.'.index'))->with('error', __('messages.'.$this->res.'.not_found'));
        }

        return view('admin.'.$this->res.'.edit')->with(compact('model', 'res','categories', 'bouquets', 'epgs', 'hosts'));
    }

    public function update($id, Request $request)
    {

        $model = Resource::where('uuid', '=', $id)->first();

        if (empty($model)) {
            return redirect(route($this->res.'.index'))->with('error', __('messages.'.$this->res.'.not_found'));
        }

        $validate = [
            'name' => 'required',
            'stream_logo' => 'required|mimes:jpeg,bmp,png,jpg',
            'category_id' => 'required|exists:category_streams,uuid',
            'link' => 'required|array|min:1',
            "link.*" => 'required|url',
            'bouquet_id' => 'array',
            'bouquet_id.*' => 'exists:bouquets,uuid'
        ];

        if(empty($request->get('stream_logo')))
            unset($validate['stream_logo']);

        $request->validate($validate);

        $input = $request->all();

        if(empty($request->get('stream_logo')))
            unset($input['stream_logo']);

        if($request->hasFile('stream_logo'))
            $input['stream_logo'] = storage($request, 'stream_logo', '/img/stream/');


        $model->fill($input);
        $model->save();

        if($model && isset($input['bouquet_id'])) {
            $relation = BouquetRelation::where(['resource_id' => $model->uuid, 'resource' => 'stream']);
            if($relation->count()) $relation->delete();

            foreach ($input['bouquet_id'] as $bouquet){
                BouquetRelation::create(['resource_id' => $model->uuid, 'resource' => 'stream', 'bouquet_id'=> $bouquet]);
            }
        }

        $links = StreamLink::where('stream_id', '=', $model->uuid);

        if($links->count()) $links->delete();

        foreach ($request->get('link') as $url) {
            StreamLink::create(['stream_id' => $model->uuid, "url" => $url]);
        }

        return redirect(route($this->res.'.index'))->with('success', __('messages.'.$this->res.'.update'));
    }


    public function destroy($id)
    {
        $models = Resource::where('uuid', '=', $id)->first();

        if (empty($models)) {
            return redirect(route($this->res.'.index'))->with('error', __('messages.'.$this->res.'.not_found'));
        }

        $models->delete();

        return redirect(route($this->res.'.index'));
    }
}
