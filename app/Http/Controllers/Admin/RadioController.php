<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\AdminRequest;
use App\Models\Bouquet;
use App\Models\BouquetRelation;
use App\Models\CategoryStream;
use App\Models\Radio as Resource;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class RadioController extends Controller
{
    private $res = 'radios';

    private $bouquets;
    private $categories;

    public function __construct()
    {
        $bouquets = Bouquet::where('status', 1)->get();
        $bouquets = $bouquets->toArray();

        $categories = CategoryStream::all();
        $categories = $categories->toArray();

        $this->bouquets =  parseSelectArray($bouquets, 'name', false,'bouquets');
        $this->categories =  parseSelectArray($categories, 'name', false,'categories');
    }

    public function index()
    {
        $res = $this->res;
        return view('admin.'.$this->res.'.index')->with(compact('res'));
    }

    public function datatable(Request $request) {

        $model = Resource::select(Resource::$cols)->get();
        return Datatables::of($model)
            ->addColumn('action', function ($model) {
                $btn = btnTableEdit($this->res.'.edit', $model);
                $btn .= btnTableDelete($this->res.'.destroy', $model);
                return $btn;
            })
            ->make(true);
    }

    public function create()
    {
        $res = $this->res;
        $bouquets = $this->bouquets;
        $categories = $this->categories;
        return view('admin.'.$this->res.'.create')->with(compact('res', 'bouquets', 'categories'));
    }

    public function store(Request $request)
    {
        $res = $this->res;
        $input = $request->all();

        $request->validate([
            'name' => 'required',
            'stream_logo' => 'required|mimes:jpeg,bmp,png,jpg',
            'stream_link' => 'required|url',
            'category_id' => 'required|exists:category_streams,uuid'
        ]);

        if($request->hasFile('stream_logo'))
            $input['stream_logo'] = storage($request, 'stream_logo', '/img/stream/');

        $radio = Resource::create($input);

        if($radio && isset($input['bouquet_id'])) {
            foreach ($input['bouquet_id'] as $bouquet){
                BouquetRelation::create(['resource_id' => $radio->uuid, 'resource' => 'radio', 'bouquet_id'=>$bouquet]);
            }
        }

        return redirect(route($this->res.'.index'))->with('success', __('messages.'.$this->res.'.created'));
    }

    public function show($id)
    {
        $res = $this->res;
        $model = Resource::where('uuid', '=', $id)->first();

        if (empty($model)) {
            return redirect(route($this->res.'.index'))->with('error', __('messages.'.$this->res.'.not_found'));
        }

        return view('admin.'.$this->res.'.show')->with(compact('model'));
    }

    public function edit($id)
    {
        $res = $this->res;
        $bouquets = $this->bouquets;
        $categories = $this->categories;
        $model = Resource::where('uuid', '=', $id)->first();

        if (empty($model)) {
            return redirect(route($this->res.'.index'))->with('error', __('messages.'.$this->res.'.not_found'));
        }

        return view('admin.'.$this->res.'.edit')->with(compact('model', 'res', 'bouquets', 'categories'));
    }

    public function update($id, Request $request)
    {
        $models = Resource::where('uuid', '=', $id)->first();
        $input = $request->all();

        if (empty($models)) {
            return redirect(route($this->res.'.index'))->with('error', __('messages.'.$this->res.'.not_found'));
        }

        $validate = [
            'name' => 'required',
            'stream_logo' => 'mimes:jpeg,bmp,png,jpg',
            'stream_link' => 'required|url',
            'category_id' => 'required|exists:category_streams,uuid'
        ];

        if(empty($request->get('stream_logo')))
            unset($validate['stream_logo']);

        $request->validate($validate);

        if(empty($request->get('stream_logo')))
            unset($input['stream_logo']);

        if($request->hasFile('stream_logo'))
            $input['stream_logo'] = storage($request, 'stream_logo', '/img/stream/');

        $models->fill($request->all());
        $models->save();

        if($models){
            $relation = BouquetRelation::where(['resource_id' => $models->uuid, 'resource' => 'radio']);
            if($relation->count()) {
                $relation->delete();
            }

            if(isset($input['bouquet_id'])) {
                foreach ($input['bouquet_id'] as $bouquet){
                    BouquetRelation::create(['resource_id' => $models->uuid, 'resource' => 'radio', 'bouquet_id'=>$bouquet]);
                }
            }
        }

        return redirect(route($this->res.'.index'))->with('success', __('messages.'.$this->res.'.update'));
    }


    public function destroy($id)
    {
        $models = Resource::where('uuid', '=', $id)->first();

        if (empty($models)) {
            return redirect(route($this->res.'.index'))->with('error', __('messages.'.$this->res.'.not_found'));
        }

        $models->delete();

        return redirect(route($this->res.'.index'));
    }
}
