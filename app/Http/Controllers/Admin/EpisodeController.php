<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\AdminRequest;
use App\Jobs\TmdbFetchEpisode;
use App\Models\Bouquet;
use App\Models\CategoryStream;
use App\Models\TvSerieEpisode;
use App\Models\TvSerieEpisode as Resource;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class EpisodeController extends Controller
{
    private $res = 'episodes';

    private $season;
    private $categories;

    public function __construct()
    {

        $bouquets = Bouquet::where('status', 1)->get();
        $bouquets = $bouquets->toArray();

        $categories = CategoryStream::all();
        $categories = $categories->toArray();

        $this->season =  parseSeasontoArray();

        $this->categories =  parseSelectArray($categories, 'name');
        $this->bouquets =  parseSelectArray($bouquets, 'name');;
    }

    public function index()
    {
        $res = $this->res;
        return view('admin.'.$this->res.'.index')->with(compact('res'));
    }

    public function datatable(Request $request) {

        $model = Resource::select(Resource::$cols)->get();
        return Datatables::of($model)
            ->addColumn('action', function ($model) {
                $btn = btnTableEdit($this->res.'.edit', $model);
                $btn .= btnTableDelete($this->res.'.destroy', $model);
                return $btn;
            })
            ->addColumn('season_no', function($model) {
                $m = TvSerieEpisode::where('uuid', '=', $model->season_id)->first();
                return $model->season->season_no . '° Temporada' ?? '';
            })
            ->editColumn('thumbnail', function ($model) {
                return imgOnTable($model->thumbnail);
            })
            ->addColumn('serie', function ($model) {
                $m = TvSerieEpisode::where('uuid', '=', $model->season_id)->first();
                return $model->season->serie->title ?? '';
            })
            ->rawColumns(['action', 'thumbnail'])
            ->make(true);
    }

    public function create()
    {
        $res = $this->res;
        $seasons = $this->season;
        $categories = $this->categories;
        return view('admin.'.$this->res.'.create')->with(compact('res', 'seasons', 'categories'));
    }

    public function store(Request $request)
    {
        $res = $this->res;
        $input = $request->all();

        $request->validate([
            'seasons_id' => 'required|exists:tv_serie_seasons,uuid',
            "episode_no" => "required",
            "episode_link" => "required",
        ]);

        $model = Resource::create($input);
        if($model) dispatch(new TmdbFetchEpisode($model->season_id, $model->uuid));

        return redirect(route($this->res.'.index'))->with('success', __('messages.'.$this->res.'.created'));
    }

    public function show($id)
    {
        $res = $this->res;
        $model = Resource::where('uuid', '=', $id)->first();

        if (empty($model)) {
            return redirect(route($this->res.'.index'))->with('error', __('messages.'.$this->res.'.not_found'));
        }

        return view('admin.'.$this->res.'.show')->with(compact('model'));
    }

    public function edit($id)
    {
        $res = $this->res;
        $model = Resource::where('uuid', '=', $id)->first();

        if (empty($model)) {
            return redirect(route($this->res.'.index'))->with('error', __('messages.'.$this->res.'.not_found'));
        }

        return view('admin.'.$this->res.'.edit')->with(compact('model', 'res'));
    }

    public function update($id, Request $request)
    {
        $models = Resource::where('uuid', '=', $id)->first();

        if (empty($models)) {
            return redirect(route($this->res.'.index'))->with('error', __('messages.'.$this->res.'.not_found'));
        }

        $models->fill($request->all());
        $models->save();

        return redirect(route($this->res.'.index'))->with('success', __('messages.'.$this->res.'.update'));
    }


    public function destroy($id)
    {
        $models = Resource::where('uuid', '=', $id)->first();

        if (empty($models)) {
            return redirect(route($this->res.'.index'))->with('error', __('messages.'.$this->res.'.not_found'));
        }

        $models->delete();

        return redirect(route($this->res.'.index'));
    }
}
