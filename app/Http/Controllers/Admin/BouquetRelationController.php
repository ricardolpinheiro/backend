<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Bouquet;
use App\Models\TvSerie;
use Illuminate\Http\Request;

class BouquetRelationController extends Controller
{

    public function index($res, $uuid){

        switch ($res) {
            case 'serie':
                return $this->series($uuid);
                break;
        }
        return '';
    }

    public function series($uuid){
        $series = TvSerie::where('uuid', '=', $uuid)->first();
        $bouquets = [];

        foreach ($series->available_bouquets as $available) {
            $bouquet = Bouquet::where('uuid', '=', $available->bouquet_id)->first();
            array_push($bouquets, $bouquet->name);
        }

        return $bouquets;
    }
}
