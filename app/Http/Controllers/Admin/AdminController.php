<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\AdminDataTable;
use App\Http\Controllers\Controller;
use App\Http\Requests\AdminRequest;
use App\Models\Admin;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\Facades\DataTables;

class AdminController extends Controller
{
    private $res = 'admins';

    public function index()
    {
        $res = $this->res;
        return view('admin.'.$this->res.'.index')->with(compact('res'));
    }

    public function datatable(Request $request) {

        $model = Admin::select(Admin::$cols)->get();
        return Datatables::of($model)
            ->addColumn('action', function ($model) {
                $btn = btnTableEdit($this->res.'.edit', $model);
                $btn .= btnTablePassword($model);
                $btn .= btnTableDelete($this->res.'.destroy', $model);
                return $btn;
            })
            ->make(true);
    }

    public function password(Request $request) {
        $request->validate(['password' => 'required|min:1', 'uuid' => 'required|exists:admins,uuid']);

        $model = Admin::where('uuid', '=', $request->get('uuid'))->first();

        $model->password = Hash::make($request->get('password'));
        $model->save();
        return response()->json(["message" => __('messages.'.$this->res.'.updated')]);
    }

    public function create()
    {
        $res = $this->res;
        return view('admin.'.$this->res.'.create')->with(compact('res'));
    }

    public function store(AdminRequest $request)
    {
        $res = $this->res;
        $input = $request->all();
        $model = Admin::create($input);
        return redirect(route($this->res.'.index'))->with('success', __('messages.'.$this->res.'.created'));
    }

    public function show($id)
    {
        $res = $this->res;
        $model = Admin::where('uuid', '=', $id)->first();

        if (empty($model)) {
            return redirect(route($this->res.'.index'))->with('error', __('messages.'.$this->res.'.not_found'));
        }

        return view('admin.'.$this->res.'.show')->with(compact('model'));
    }

    public function edit($id)
    {
        $res = $this->res;
        $model = Admin::where('uuid', '=', $id)->first();

        if (empty($model)) {
            return redirect(route($this->res.'.index'))->with('error', __('messages.'.$this->res.'.not_found'));
        }

        return view('admin.'.$this->res.'.edit')->with(compact('model', 'res'));
    }

    public function update($id, Request $request)
    {
        $models = Admin::where('uuid', '=', $id)->first();

        if (empty($models)) {
            return redirect(route('admins.index'))->with('error', __('messages.'.$this->res.'.not_found'));
        }

        $models->fill($request->all());
        $models->save();

        return redirect(route($this->res.'.index'))->with('success', __('messages.'.$this->res.'.update'));
    }


    public function destroy($id)
    {
        $models = Admin::where('uuid', '=', $id)->first();

        if (empty($models)) {
            return redirect(route($this->res.'.index'))->with('error', __('messages.'.$this->res.'.not_found'));
        }

        $models->delete();

        return redirect(route($this->res.'.index'));
    }
}
