<?php

namespace App\Http\Controllers;

use App\Export;
use App\Jobs\TmdbFetchMovie;
use App\Models\Bouquet;
use App\Models\Movie;
use App\Models\Stream;
use App\Models\StreamLink;
use App\Models\StreamStatus;
use App\Models\TvSerie;
use App\Models\TvSerieSeason;
use App\TempSerie;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\URL;
use Symfony\Component\Process\Process;
use function GuzzleHttp\Psr7\str;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('admin.dashboard');
    }

    public function teste() {
        $bouquets = [];
        set_time_limit(30000);
        ini_set('auto_detect_line_endings',TRUE);
        ini_set('max_execution_time ',1000000);

        $data = Export::all();


//        foreach ($b as $item) {
//            $bouquet = Bouquet::where('name', '=', trim($item->bouquet_name))->first();
//
//            if(!$bouquet) continue;
//
//            $arr_channel = explode(',', str_replace(']', '', str_replace('[', '', $item->bouquet_channels)));
//            $arr_serie = explode(',', str_replace(']', '', str_replace('[', '', $item->bouquet_series)));
//
//            $bouquets[$bouquet->uuid]['id'] = $item->id;
//            $bouquets[$bouquet->uuid]['channel'] = $arr_channel;
//            $bouquets[$bouquet->uuid]['serie'] = $arr_serie;
//        }
        foreach ($data as $item) {
            switch ($item->type_key){
                case 'live':
                    //$this->addLive($item);
                    break;
                case 'movie':
                    $this->addMovie($item);
                    break;
                case 'series':
                    //$this->addSerie($item);
                    break;
            }
        }
    }

    public function addMovie($item) {
        $tmdb = str_replace("\"", "", str_replace("{kinopoisk_url\":\"https:\/\/www.themoviedb.org\/movie\/", "", $item->movie_propeties));
        $chave = explode(",", $tmdb);
        $id = '';

        if(is_array($chave)){
            foreach ($chave as $v) {
                if (strpos($v, 'tmdb_id') >= 0){
                    $id = (int) str_replace("\"", "", str_replace('tmdb_id:', '', $v));
                }
            }
        } else {
            $id = (int) $chave;
        }

        if(!is_int($id)) dd($id);

        $link = "http://51.79.82.9:25461/movie/ricpeludo/288299/"
                        . str_replace("\"", "", $item->id)
                        . substr(substr($item->stream_source, (strlen($item->stream_source)-7), strlen($item->stream_source)), 0, 4);

        $movie = new Movie();
        $movie->title = $item->stream_display_name;
        $movie->tmdb_id = $id;
        $movie->movie_link = $link;
        $movie->save();
        Log::info($movie->uuid);
        dispatch(new TmdbFetchMovie($movie));
    }

    public function addSerie($item){
        $serie=[];
        $explode = explode('-', $item->stream_display_name);

        foreach ($explode as $key => $sentence) {
            $explode[$key] = trim($sentence);
        }

        if(count($explode) >= 3 && strpos($explode[1], 'S') === 0){
            $info = explode("E", str_replace('S', '', $explode[1]));
        }

        if(count($explode) === 2 && strpos($explode[1], 'S') === 0){
            $info = explode("E", str_replace('S', '', $explode[1]));
        }

        if(count($explode) == 3 && strpos($explode[1], 'S') === 0) {
            $info = explode("E", str_replace('S', '', $explode[1]));
        }

        if(count($explode) > 3 && strpos($explode[2], 'S') === 0) {
            $info = explode("E", str_replace('S', '', $explode[2]));
        }

        if(!isset($info) || !isset($info[1])){
            return;
        }
//        http://tau.selfip.com:25461/series///14102.mkv
        if(trim($explode[0]) === 'He') {
            $name = trim($explode[0]) . "-" . trim($explode[1]);
        } elseif (trim($explode[0]) === 'She') {
            $name = trim($explode[0]) . "-" .trim($explode[1]);
        } elseif (trim($explode[0]) === 'X') {
            $name = trim($explode[0]) . "-" .trim($explode[1]);
        } elseif (trim($explode[0]) === 'Taz') {
            $name = trim($explode[0]) . "-" .trim($explode[1]);
        }elseif (trim($explode[0]) === 'Manda') {
            $name = trim($explode[0]) . "-" .trim($explode[1]);
        }elseif (trim($explode[0]) === 'Marvel') {
            $name = trim($explode[0]) . "-" .trim($explode[1]);
        }else {
            $name = trim($explode[0]);
        }

        $link = "http://51.79.82.9:25461/series/ricpeludo/288299/"
            . str_replace("\"", "", $item->id)
            . substr(substr($item->stream_source, (strlen($item->stream_source)-7), strlen($item->stream_source)), 0, 4);

//        $item->title = $name;
//        $item->tmdb_id = '';
//        $item->season = $info[0];
//        $item->episode = $info[1];
//        $item->link = $link;
//        $item->save();


        $serie['name'] = $name;
        $serie['episodio'] = $info[1];
        $serie['temporada'] = $info[0];
        $serie['link'] = $link;


        $seriedb = checkExistsSerie($serie);

        if($seriedb instanceof TvSerie) {
            $seasondb = checkExistsSerieSeason($seriedb, $serie);
            if($seasondb instanceof TvSerieSeason) {
                $episode = checkExistsSerieSeasonepisode($seasondb, $serie);
            }
        }
    }

    public function addLive($item) {
        $stream = new Stream();
        $stream->name = $item->stream_display_name;
        $stream->stream_logo = filter_var($item->stream_icon, FILTER_VALIDATE_URL) ? storage_resource($item->stream_icon, PATH_RESOURCE_STREAMS) : URL::to(public_path('img/electronics.png'));;
        $stream->save();

        $links = str_replace("\"", "", str_replace("]", "", str_replace("[", "", str_replace('\\', '', $item->stream_source))));
        $links = explode(',', $links);

        foreach ($links as $l) {
            $stream_link = new StreamLink();
            $stream_link->stream_id = $stream->uuid;
            $stream_link->url = $l;
            $stream_link->save();
        }

    }
}
