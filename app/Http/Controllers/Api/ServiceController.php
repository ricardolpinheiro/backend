<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Movie;
use App\Models\Stream;
use App\Models\TvSerie;
use Illuminate\Http\Request;

class ServiceController extends Controller
{

    public function login(){}

    public function fetch() {
        $revenda = [''];
        $bouquets_revenda = [''];
        $movies = [''];
        $series = [''];
        $streams = [];
        $radios = [];
        $home_banner = [];
        $home_stream = [];

        $movies = Movie::all();
        $series = TvSerie::with('seasons.episodes')->get();
        $streams = Stream::all();

        return [
            'data'=> [
                'revenda'=> "01",
                'home_banner' => [
                    'http://hiperxp.com.br/banner/Sem%20T%C3%ADtulo-1.jpg',
                    'http://hiperxp.com.br/banner/Sem%20T%C3%ADtulo-2.jpg',
                    'http://hiperxp.com.br/banner/Sem%20T%C3%ADtulo-3.jpg',
                    'http://hiperxp.com.br/banner/Sem%20T%C3%ADtulo-4.jpg',
                    'http://hiperxp.com.br/banner/Sem%20T%C3%ADtulo-5.jpg'
                ],
                'home_stream' => "http://brzstream.dyndns.org:25461/BRZ/45rtfgvb/159",
                'stream' => $streams,
                'movie' => $movies,
                'serie' => $series
            ]
        ];
    }

}
