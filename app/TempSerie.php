<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TempSerie extends Model
{
    public $timestamps = false;
    protected $fillable = [
        "title",
        "tmdb_id",
        "season",
        "episode",
        "link",
    ];

}
