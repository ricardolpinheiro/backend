<?php

namespace App\View\Components;

use Illuminate\View\Component;

class InputPrepend extends Component
{
    public $name;
    public $text;
    public $placeholder;
    public $icon;
    public $required;
    public $error;

    public $message;
    public $hasError;
    public $value;
    public $prepend;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($name, $prepend="" ,$text='', $placeholder='', $icon='', $error = null, $required=false, $value='')
    {

        $this->name = $name;
        $this->text = empty($text) ? ucfirst($name) : $text;
        $this->placeholder = empty($placeholder) ? ucfirst($name) : $placeholder;
        $this->icon = $icon;
        $this->prepend = $prepend;
        $this->required = $required;

        $this->message = $this->getMessage($name, $error->getMessageBag());
        $this->hasError = !empty($this->message) ? true : false;
        $this->value = $value;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {

        return view('components.input-prepend');
    }

    public function getMessage($name, $errors=[]) {
        $keys = array_keys($errors->messages());
        $pos = in_array($name, $keys);

        if($pos) {
            return $errors->get($name)[0];
        }
    }
}
