<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Button extends Component
{
    public $route;
    public $tooltip;
    public $icon;
    /**
     * @var string
     */
    public $type;
    public $id;
    /**
     * @var string
     */
    public $value;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($route='', $tooltip, $icon, $value='',$id='',$type='primary')
    {
        //
        $this->route = $route;
        $this->tooltip = $tooltip;
        $this->icon = $icon;
        $this->type = $type;
        $this->id = $id;
        $this->value = $value;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.button');
    }
}
