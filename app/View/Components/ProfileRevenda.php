<?php

namespace App\View\Components;

use Illuminate\View\Component;

class ProfileRevenda extends Component
{
    public $revenda;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($revenda)
    {
        //
        $this->revenda = $revenda;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.profile-revenda');
    }
}
