<?php

namespace App\View\Components;

use Illuminate\View\Component;

class InputFile extends Component
{
    public $name;
    public $text;
    public $placeholder;
    public $icon;
    public $required;
    public $error;

    public $message;
    public $hasError;
    public $value;
    /**
     * @var string
     */
    public $display;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($name, $text='', $placeholder='', $display='',$icon='', $error = null, $required=false, $value='')
    {

        $this->name = $name;
        $this->text = empty($text) ? ucfirst($name) : $text;
        $this->placeholder = empty($placeholder) ? ucfirst($name) : $placeholder;
        $this->icon = $icon;
        $this->required = $required;

        $this->display = $display == 'none' ? 'none' : '';
        $this->message = $this->getMessage($name, $error->getMessageBag());
        $this->hasError = !empty($this->message) ? true : false;
        $this->value = $value;
    $this->display = $display;}

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.input-file');
    }

    public function getMessage($name, $errors=[]) {
        $keys = array_keys($errors->messages());
        $pos = in_array($name, $keys);

        if($pos) {
            return $errors->get($name)[0];
        }
    }
}
