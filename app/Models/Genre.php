<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Genre extends Base
{
    protected $fillable = [
        'name',
        'tmdb_id'
    ];
}
