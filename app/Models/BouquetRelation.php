<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BouquetRelation extends Base
{

    protected $fillable = [
        'bouquet_id',
        'resource_id',
        'resource',
    ];

}
