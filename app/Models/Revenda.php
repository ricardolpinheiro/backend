<?php

namespace App\Models;

use App\Jobs\CreateRevendaPanel;
use Illuminate\Database\Eloquent\Model;
use Webpatser\Uuid\Uuid;

class Revenda extends Base
{

    protected $fillable = [
        'uuid',
        'name',
        'username',
        'obs',
        'password',
        'credits',
        'has_painel',
        'limit_users',
        'path_logo',
        'path_background',
        'url_painel',
        'app_name',
        'app_url',
        'primary_color',
        'secondary_color',
        'screen_count',
        'status',
        'own_id',
        'created_by',
    ];

    public static $cols = ['uuid', 'name', 'username'];

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->uuid = (string) Uuid::generate(4);
        });

        self::created(function ($model) {
            dispatch(new CreateRevendaPanel($model));
        });
    }

}
