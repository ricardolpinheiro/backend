<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StreamLink extends Base
{

    protected $fillable = ["stream_id", "url"];

    public static $cols = ["stream_id", "url"];

    public function status() {
        return $this->hasOne(StreamStatus::class, 'link_id', 'uuid');
    }
}
