<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MovieCollection extends Base
{
    protected $fillable = [
        "name",
        "overview",
        "poster_path",
        "backdrop_path",
        "tmdb_id",
    ];

    public function movie() {
        return $this->hasMany(Movie::class, 'collection_id', 'id' );
    }
}
