<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategoryStream extends Base
{

    protected $fillable = ['name'];

    public static $cols = ['uuid', 'name'];

}
